use std::process::Command;

use winapi::{
    shared::minwindef::{BOOL, DWORD, HINSTANCE, LPVOID, TRUE},
    um::winnt::DLL_PROCESS_ATTACH,
};

#[no_mangle]
extern "system" fn DllMain(_: HINSTANCE, call_reason: DWORD, _: LPVOID) -> BOOL {
    match call_reason {
        DLL_PROCESS_ATTACH => {
            Command::new("calc.exe").arg("").status().unwrap();
        }
        _ => {}
    }

    TRUE
}
