# Pop Calc DLL
A DLL that pops calc... For DLL hijacking testing purposes.

## Building on Windows
1. `cargo build --release`
2. result at `./target/release/pop_calc.dll`

## Building on not-Windows
1. install mingw
2. `rustup target add x86_64-pc-windows-gnu`
3. `cargo build --release --target x86_64-pc-windows-gnu`
4. result at `./target/x86_64-pc-windows-gnu/release/pop_calc.dll`